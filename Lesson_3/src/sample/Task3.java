package sample;

import java.util.Scanner;

public class Task3 {
	
	public static void main(String[] args) {

		/* Треугольник существует только тогда, когда сумма любых двух его сторон больше третьей. Дано: a,
		b, c – стороны предполагаемого треугольника. Напишите программу, которая укажет, существует ли
		такой треугольник или нет. */
		
		Scanner keyWord = new Scanner(System.in);
		
		System.out.println("Side a: ");
		int a = keyWord.nextInt();
		System.out.println("Side b: ");
		int b = keyWord.nextInt();
		System.out.println("Side c: ");
		int c = keyWord.nextInt();
		
		
		if ((a+b)>c && (a+c)>b && (c+b)>a) {
			System.out.println("Такий трикутник існує!");
		} else {
			System.out.println("Такого трикутника не існує!");
		}
		
	}
}
