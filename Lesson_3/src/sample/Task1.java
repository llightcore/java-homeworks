package sample;

import java.util.Scanner;

public class Task1 {
	
	public static void main(String[] args) {
		
		/* Написать программу которая считает 4 целых числа с клавиатуры и выведет на экран самое
		большое из них. */
		
		Scanner keyWord = new Scanner(System.in);
		
		int number1;
		int number2;
		int number3;
		int number4;
		int maxNumber;
		
		System.out.println("Number1: ");
		number1 = keyWord.nextInt();
		System.out.println("Number2: ");
		number2 = keyWord.nextInt();
		System.out.println("Number3: ");
		number3 = keyWord.nextInt();
		System.out.println("Number4: ");
		number4 = keyWord.nextInt();
		
		maxNumber = number1;
		if (number2 > maxNumber) {
			maxNumber = number2;
		} 
		if (number3 > maxNumber) {
			maxNumber = number3;
		} 
		if (number4 > maxNumber) {
			maxNumber = number4;
		}
		System.out.println("Найбільше число: " + maxNumber);
	}

}
