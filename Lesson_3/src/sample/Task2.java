package sample;

import java.util.Scanner;

public class Task2 {
	
	public static void main(String[] args) {
		
		/* Есть девятиэтажный дом, в котором 5 подъездов. Номер подъезда начинается с единицы. На
		одном этаже 4 квартиры. Напишите программу которая, получит номер квартиры с клавиатуры, и
		выведет на экран, на каком этаже, какого подъезда расположена эта квартира. Если такой
		квартиры нет в этом доме, то нужно сообщить об этом пользователю. */
		
		Scanner keyWord = new Scanner(System.in);
		
		int mainFloors = 9; // Маємо всього поверхів
		int entrances = 5;  // Кількість під'їздів у поверхівці
		int flatsInFloor = 4; // Квартир на одному поверсі
		int flatsInEnter = mainFloors * flatsInFloor; // 36 квартир в одному під'їзді
		int flatsSum = flatsInEnter * entrances; // 180 квартир всього на усій поверхнівці
		
		System.out.println("Введіть номер квартири: ");
		int flatNumber = keyWord.nextInt(); 
		
		if (flatNumber > 0 && flatNumber <= flatsSum) {
			 
			System.out.println("Ваш під'їзд: №" + ((flatNumber - 1) / flatsInEnter + 1)); 
			/* Додаємо +1 так як ведемо розрахунок від 1, а не 0 */
			
			System.out.println("Ваш поверх: " + (((flatNumber - 1) % flatsInEnter) / flatsInFloor + 1));
			/* Розрахунок поверхів також від 1, а не 0 */
			
		} else { System.out.println("Не існує такої квартири!"); }
		
	}

}
