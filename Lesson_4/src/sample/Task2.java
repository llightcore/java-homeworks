package sample;

import java.util.Scanner;

public class Task2 {
	
	public static void main(String[] args) {
		
		/* Напечатайте таблицу умножения на 5. предпочтительно печатать 1 x 5 = 5, 2 x 5 = 10, а не просто 5,
		10 и т. д. */
		
		Scanner key = new Scanner(System.in);
		
		System.out.println("Таблиця множення на 5");
		
		for(int i = 1; i <= 9; i++) {
			int result = i * 5;
			System.out.println(i + " x 5 = " + result);
		}
		
	}

}
