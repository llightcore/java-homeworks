package sample;

import java.util.Scanner;

public class Task1 {
	
	public static void main(String[] args) {
		
		/* Вычислить с помощью цикла факториал числа - n введенного с клавиатуры (4<n<16). Факториал
		числа это произведение всех чисел от этого числа до 1. Например 5!=5*4*3*2*1=120 */
		
		Scanner key = new Scanner(System.in);
		
		System.out.println("Знайдемо факторіал введеного вами числа:");
		int n = key.nextInt();
		long result = 1;
		
		if(n > 4 && n < 16) {
			
		/*	for(;;) {
				if(n <= 1) {
					break;
				}
				result *= n * (n - 1);
				n -= 2;
			} 
		*/
			
			while(n >= 1) {
				result *= n;
				n--;
			}
			
			System.out.println("Відповідь: " + result);
			
		} else {
			System.out.println("Введіть число в діапазоні (4<n<16)!");
		}
		
	}

}
