package sample;

import java.util.Scanner;

public class Task3 {
	
	public static void main(String[] args) {
		
		/* Выведите на экран прямоугольник из *. Причем высота и ширина прямоугольника вводятся с
		клавиатуры. Например ниже представлен прямоугольник с высотой 4 и шириной 5.
		*****
		*   *
		*   *
		*****
		*/
		
		Scanner key = new Scanner(System.in);
		
		System.out.println("Width: ");
		int width = key.nextInt();
		System.out.println("Heigh: ");
		int heigh = key.nextInt();
		
		for(int i = 0; i < heigh; i++) {
			for(int j = 0; j < width; j++) {
				if((i == 0 || i == heigh - 1) || (j == 0 || j == width - 1)) {
					System.out.print("*");
				} else {
					System.out.print(" ");
				}
			}
			System.out.println();
		}
	}

}
