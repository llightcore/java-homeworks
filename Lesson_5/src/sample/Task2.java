package sample;

import java.util.Arrays;
import java.util.Scanner;

public class Task2 {
	public static void main(String[] args) {
		/* Написать код для возможности создания массива целых чисел (размер вводиться с клавиатуры) и
		возможности заполнения каждого его элемента вручную. Выведите этот массив на экран. */
		
		Scanner key = new Scanner(System.in);
		
		int[] arrayOne;
		int size;
		System.out.println("Введіть розмір масиву: ");
		
		while(true) {
			size = key.nextInt();
			if (size > 20 || size < 0) {
				System.out.println("Введіть число в діапазоні 0 < а <= 20");
			} else break;
		}
		
		arrayOne = new int[size];
		 
		for(int i = 0; i < arrayOne.length; i++) {
			
			System.out.println("Введіть елемент по індексу " + i);
				arrayOne[i] = key.nextInt();
		}
			
		System.out.println("Ви задали такий масив: " + Arrays.toString(arrayOne));
		
	} 

}
