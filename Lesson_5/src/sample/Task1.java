package sample;

public class Task1 {
	public static void main(String[] args) {
		/* Дан массив целых чисел вида — {0,5,2,4,7,1,3,19}. Написать программу для подсчета количества
		нечетных чисел в нем. */
		
		int arrayOne[] = {0,5,2,4,7,1,3,19};
		
		int result = 0;
		
		for(int i = 0; i < arrayOne.length; i++) {
			if(arrayOne[i] % 2 != 0) {
				result += 1;
			}
		}
		System.out.print("Сума непарних чисел: " + result);
	}

}
