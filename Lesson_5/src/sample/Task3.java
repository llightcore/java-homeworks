package sample;

import java.util.Arrays;
import java.util.Random;

public class Task3 {
	public static void main(String[] args) {
		/* Создать массив случайных чисел (размером 15 элементов). Создайте второй массив в два раза
		больше, первые 15 элементов должны быть равны элементам первого массива, а остальные
		элементы заполнить удвоенных значением начальных. Например:
		Было → {1,4,7,2}
		Стало → {1,4,7,2,2,8,14,4} */
		
		Random random = new Random();
		
		int[] arrayOne = new int[15];
		int[] arrayTwo = new int[30];
		
		for(int i = 0; i < arrayOne.length; i++) {
			arrayOne[i] = random.nextInt(15);
		}
		
		arrayTwo = Arrays.copyOfRange(arrayOne, 0, 30);
		
		int index = 0;
		for(int i = 15; i < 30; i++) {
			arrayTwo[i] = arrayOne[index] * 2;
			index += 1;
		}
		
		System.out.println("Перший масив: " + Arrays.toString(arrayOne));
		System.out.println("Другий масив: " + Arrays.toString(arrayTwo));
	}

}
