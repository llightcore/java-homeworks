package sample;

public class Task2 {

	public static void main(String[] args) {
		
		/* Стоимость яблока составляет 2$. Покупатель приобретает 6 яблок. Напишите
		программу которая вычислит и выведет на экран сумму которую должен уплатить
		покупатель за покупку.  */
		
		
		int applePrice = 2;
		int buy = 6;
		
		int sum = applePrice * buy;
		
		System.out.println("Повинен оплатити: " + sum + "$");
		
	}
	
}
