package sample;

import java.util.Scanner;

public class Task3 {
	
	public static void main(String[] args) {
		
		/* Один литр топлива стоит 1.2$. Ваш автомобиль тратит на 100 км пути 8 литров топлива.
		Вы собрались в поездку в соседний город. Расстояние до этого города составляет 120
		км. Вычислите и выведите на экран сколько вам нужно заплатить за топливо для
		поездки. */
		
		Scanner press = new Scanner(System.in);
		
		double fuelPrice = 1.2;
		System.out.println("Ціна бензину за 1л: " + fuelPrice + "($)");
		
		double mainKm = 100;
		double mainLiter = 8;
		
		double kmPerLiter = mainKm / mainLiter;
		System.out.println("Проїжджає за 1л бензину: " + kmPerLiter + "(km)");
		
		System.out.println("Вкажіть відстань до вашого пункту призначення:");
		double dist = press.nextInt();
		
		double literPerDist = dist / kmPerLiter;
		System.out.println("Витрачає бензину за вказаною відстанню: " + literPerDist + "(L)");
		
		double needToPay = literPerDist * fuelPrice;
		System.out.println("Вам необхідно сплатити: " + needToPay + "($)");
		
	}

}
