package sample;

public class Task1 {

	public static void main(String[] args) {
		
		/* Написать программу которая вычислит и выведет на экран площадь треугольника если
		известны его стороны (sideA = 0.3, sideB = 0.4, sideC = 0.5). Для вычисления
		использовать формулу Герона. */
		
		
		double sideA = 0.3;
		double sideB = 0.4;
		double sideC = 0.5;
		
		double halfP;
		halfP = (sideA + sideB + sideC) / 2;
		
		System.out.println("Півпериметр: " + halfP);
		
		double area = halfP * (halfP - sideA) * (halfP - sideB) * (halfP - sideC);
		area = Math.sqrt(area);
		
		System.out.println("Площа трикутника за формулою: " + area);
		
	}
	
}
